checkdnsbls
===========

Checks the status of one or more ip addresses on multiple dns
blacklists.  DNS blacklists are typically used for spam filtering, but
there are other ones (like the ones hosted at rfc-clueless.org) that
indicate that the owner of an IP address doesn't respect the customs
of internet server administration.

Invocation
----------
::
  checkdnsbls --blacklistfile file-containing-dnsbls.txt example.net [example.org [...]]

What you might want to use it for
---------------------------------
It's quite easy to, accidentally or erroneously, get a listing on one
of the major email spam blacklists.  If you want outgoing mail from
your server to arrive, it's a good idea to remain off them.
checkdnsbls can be used to periodically check the status of
domains/ips you manage, and take action if one of your ips gets
listed.
