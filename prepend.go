package main

import (
	"fmt"
	"net"
	"strconv"
	"strings"
)

func Prepend(addr net.IP) (string, error) {
	if addr.To4() != nil {
		var sb strings.Builder
		octets := strings.Split(addr.String(), ".")
		for i := len(octets) - 1; i >= 0; i-- {
			sb.WriteString(octets[i])
			sb.WriteString(".")
		}
		return sb.String(), nil
	} else if addr.To16() != nil {
		buf := make([]byte, 0, 64)
		for i := len(addr) - 1; i >= 0; i-- {
			buf = strconv.AppendUint(buf, uint64(addr[i]&0x0f), 16)
			buf = append(buf, '.')
			buf = strconv.AppendUint(buf, uint64(addr[i]>>4), 16)
			buf = append(buf, '.')
		}
		return string(buf), nil
	} else {
		return "", fmt.Errorf("Prepend only supports ipv4 and ipv6 addresses")
	}

}
