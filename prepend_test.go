package main

import (
	"net"
	"testing"
)

func TestPrepend(t *testing.T) {
	tests := []struct {
		in       net.IP
		expected string
	}{
		{
			net.ParseIP("131.155.2.3"),
			"3.2.155.131.",
		},
		{
			net.ParseIP("fe80::"),
			"0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.8.e.f.",
		},
		{
			net.ParseIP("2600:1234::f03c:91ff:fe50:d2"),
			"2.d.0.0.0.5.e.f.f.f.1.9.c.3.0.f.0.0.0.0.0.0.0.0.4.3.2.1.0.0.6.2.",
		},
	}

	for ti := range tests {
		out, err := Prepend(tests[ti].in)
		if err != nil {
			t.Errorf("unexpected error from Prepend(): %s", err)
			continue
		}
		if out != tests[ti].expected {
			t.Errorf("error: expected `%s`, got `%s`", tests[ti].expected, out)
		}
	}
}
