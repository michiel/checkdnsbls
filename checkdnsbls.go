// Check known DNS blacklists concurrently to see if
// one of the addresses specified on the command line
// is known to them.  If so, output something.  If not,
// output nothing
package main

// Copyright 2013-2022, Michiel Buddingh, All rights reserved.
// Use of this code is governed by version 2.0 or later of the Apache
// License, available at http://www.apache.org/licenses/LICENSE-2.0

import (
	"bufio"
	"context"
	"flag"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"os/signal"
	"strings"
	"sync"
	"time"

	"golang.org/x/sync/errgroup"
)

func lookup(ctx context.Context, out *log.Logger, r *net.Resolver, hostname, dnsbl string, addr net.IP) error {
	var sb strings.Builder
	prepended, err := Prepend(addr)
	if err != nil {
		return fmt.Errorf("could not convert ip address: %w", err)
	}
	sb.WriteString(prepended)
	sb.WriteString(dnsbl)
	toLookup := sb.String()
	response, err := r.LookupHost(ctx, toLookup)
	if err == nil && net.ParseIP(response[0]).To4()[3] != 0 {
		out.Printf("%s (%s) is blacklisted by %s", addr, hostname, dnsbl)
	}
	return nil
}

func resolveTargetAddresses(ctx context.Context, r *net.Resolver, names []string) map[string][]string {
	localAddresses := make(map[string][]string)

	group, firstFailure := errgroup.WithContext(ctx)
	group.SetLimit(20)
	mut := sync.Mutex{}
	for ni := range names {
		hostname := names[ni]
		group.Go(func() error {
			addr, err := r.LookupHost(firstFailure, hostname)
			if err == nil {
				mut.Lock()
				defer mut.Unlock()
				localAddresses[hostname] = append(localAddresses[hostname], addr...)
				return nil
			} else {
				return fmt.Errorf("could not resolve name `%s`: %w", hostname, err)
			}
		})

	}
	if err := group.Wait(); err != nil {
		log.Fatalf("%s", err)
	}

	return localAddresses
}

func readBlacklistFile(blacklistfile string) ([]string, error) {
	if blacklistfile == "" {
		return nil, fmt.Errorf("must provide blacklistfile")
	}
	f, err := os.Open(blacklistfile)
	if err != nil {
		return nil, fmt.Errorf("could not open blacklistfile `%s`: %w", blacklistfile, err)
	}
	defer f.Close()
	bf := bufio.NewReader(f)
	lines := make([]string, 0, 100)

	for {
		line, err := bf.ReadString('\n')
		line = strings.Trim(line, "\n \t")
		lines = append(lines, line)
		if err == io.EOF {
			break
		}
		if err != nil {
			return lines, fmt.Errorf("error encountered reading blacklistfile `%s`: %w", blacklistfile, err)
		}
	}

	return lines, nil
}

type timeoutFlag struct {
	time.Duration
}

func (t *timeoutFlag) Set(raw string) error {
	var err error
	t.Duration, err = time.ParseDuration(raw)
	return err
}

func (t *timeoutFlag) String() string {
	return t.Duration.String()
}

func main() {
	ctx, cancelSigHandler := signal.NotifyContext(context.Background(), os.Interrupt)
	defer cancelSigHandler()
	resolver := &net.Resolver{PreferGo: true}
	outBuffer := bufio.NewWriter(os.Stdout)
	out := log.New(outBuffer, "", 0)
	timeoutFlag := timeoutFlag{}
	var blacklistfile string

	flag.Var(&timeoutFlag, "timeout", "timeout for all lookups")
	flag.StringVar(&blacklistfile, "blacklistfile", "", "list of dns blacklists, one per line")
	flag.Parse()

	if timeoutFlag.Duration != 0 {
		var cancelFunc func()
		ctx, cancelFunc = context.WithTimeout(ctx, timeoutFlag.Duration)
		defer cancelFunc()
	}

	dnsbls, err := readBlacklistFile(blacklistfile)
	if err != nil {
		log.Fatalf("%s\n", err)
	}

	localAddresses := resolveTargetAddresses(ctx, resolver, flag.Args())

	group, _ := errgroup.WithContext(ctx)
	group.SetLimit(20)
	for name := range localAddresses {
		for ai := range localAddresses[name] {
			for di := range dnsbls {
				dnsbl := dnsbls[di]
				ip := net.ParseIP(localAddresses[name][ai])
				group.Go(func() error {
					return lookup(ctx, out, resolver, name, dnsbl, ip)
				})
			}
		}
	}

	group.Wait()
	outBuffer.Flush()
}
